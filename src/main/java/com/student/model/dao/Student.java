package com.student.model.dao;

public class Student {
	private int id;
	private String name;
	private String subname;
	private int marks;
	private int testnumber;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubname() {
		return subname;
	}

	public void setSubname(String subname) {
		this.subname = subname;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public int getTestnumber() {
		return testnumber;
	}

	public void setTestnumber(int testnumber) {
		this.testnumber = testnumber;
	}

	public Student(int id, String name, String subname, int marks, int testnumber) {

		this.id = id;
		this.name = name;
		this.subname = subname;
		this.marks = marks;
		this.testnumber = testnumber;
	}

	public Student() {

		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", subname=" + subname + ", marks=" + marks + ", testnumber="
				+ testnumber + "]";
	}

}