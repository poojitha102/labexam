package com.student.model.dao;

import java.util.*;
import java.util.Map.Entry;

public class TestDemo {
	public static void main(String[] args) {
		StudentDao studentdao = new StudentDaoImpl();
		
		List<Student> studentList = studentdao.getAllStudents();
		//studentList.forEach(s -> System.out.println(s));
		//Map<Integer,Student> map = new HashMap<>();
		
		Map<Integer,String> studentsMap= new HashMap<Integer,String>();
		studentsMap.put(76, "pooja");
		studentsMap.put(25, "giri");
		studentsMap.put(45, "kushi");
		studentsMap.put(32, "keerti");
		studentsMap.put(56, "jaanu");
		Set<Entry<Integer,String>> es = studentsMap.entrySet();
		System.out.println("sorted as per marks");
		es.stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);
		
		
		System.out.println("sorted as per names");
		es.stream().sorted(Map.Entry.comparingByValue()).forEach(System.out::println);
		
		//studentsMap.forEach((mark,name)-> System.out.println(mark + ":" + name));
	

}
}
